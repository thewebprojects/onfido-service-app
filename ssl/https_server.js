var https = require('https');

var fs = require('fs');

var https_options = {

  key: fs.readFileSync("/home/ubuntu/onfido/ssl/id.trellisplatform.key"),

  cert: fs.readFileSync("/home/ubuntu/onfido/ssl/crt_code.crt"),

  ca: [

          fs.readFileSync('/home/ubuntu/onfido/ssl/CA_root.crt'),

          fs.readFileSync('/home/ubuntu/onfido/ssl/ca_bundle_certificate.crt')

       ]
};


https.createServer(https_options, function (req, res) {

 res.writeHead(200);

 res.end("Welcome to Node.js HTTPS Servern");

}).listen(443)

