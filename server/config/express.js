
import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import cors from 'cors';
import helmet from 'helmet';
import compression from 'compression';
import methodOverride from 'method-override';
import constant from '../config/directory';
import fs from 'fs';
import path from 'path';
import https from 'https';
import sslRootCasLatest from 'ssl-root-cas/latest';
import socketIo from 'socket.io';

require('dotenv').config();
require('https');

const app = express();
https.globalAgent.options.ca = sslRootCasLatest.create();

const ssl_options = {
  key: fs.readFileSync('/home/ubuntu/onfido/ssl/id.trellisplatform.key'),
  cert: fs.readFileSync('/home/ubuntu/onfido/ssl/crt_code.crt'),
  ca: [
          fs.readFileSync('/home/ubuntu/onfido/ssl/CA_root.crt'),

          fs.readFileSync('/home/ubuntu/onfido/ssl/ca_bundle_certificate.crt')

       ]
};

app.set('port', process.env.APP_PORT || 443);
app.set('host', process.env.APP_HOST || 'https://id.trellisplatform.com');

const server = https.createServer(ssl_options, app);

console.log(`============> Listening on ${process.env.APP_HOST}:${process.env.APP_PORT}`);

const io = socketIo(server);
server.listen(app.get('port'), app.get('host'));

io.on('connection', socket => {
  console.log('User connected');
  socket.on('disconnect', () => {
    console.log('User disconnected');
  });
});

app.use(express.static(constant.distDir));

app.use(cors());
app.use(helmet());
app.use(compression());
app.use(methodOverride());

app.use((req, res, next) => {
  res.io = io;
  next();
});

app.use(
  bodyParser.json({
    verify: (req, res, buf, encoding) => {
      if (buf && buf.length) {
        req.rawBody = buf.toString(encoding || 'utf8');
      }
    },
  })
);

app.use(morgan('dev'));
app.use(express.static(constant.assetsDir));

export default app;
