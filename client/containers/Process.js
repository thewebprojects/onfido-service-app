import React, { Component } from 'react';
import Layout from '../components/Layout';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Link } from 'react-router-dom';
import { init } from 'onfido-sdk-ui';
import api from '../utils/api';
import axios from 'axios';

import socketIOClient from 'socket.io-client';
import { HOST } from '../config/config';

const socket = socketIOClient(HOST);

class Process extends Component {
  constructor(props) {
    super(props);

    this.state = {
      is_expired: false,
      is_reviewing: false,
      is_waiting: false,
      is_done: false,

      firstName: '',
      middleName: '',
      lastName: '',
      address1: '',
      address2: '',
      address3: '',
      dob: '',
      document: '',
      city: '',
      state: '',
      zip: '',
    };
  }

  setWaiting = () => {
    this.setState({ is_waiting: true });
  };

  componentDidMount() {
    let self = this;

    api.get(`/onfido-init/${this.props.match.params.token}`).then((res) => {
      if (res.data.error) {
        this.setState({ is_expired: true });
        return;
      }

      init({
        token: res.data.sdk_token,
        containerId: 'onfido-mount',
        steps: [
          {
            type: 'welcome',
            options: {
              title: 'Clear List',
              descriptions: [
                'ClearList requires our clients to register for a Trellis digital identification profile.',
                'This simple process will enable our clients to streamline private-market transactions in a secure manner.',
              ],
              nextButton: "LETS'S GET STARTED",
            },
          },
          'document',
          'face',
        ],
        onComplete: function (data) {
          api.get(`/onfido-check/${res.data.applicant_id}`).then((res) => {
            return self.setWaiting();
          });
        },
      });
    });

    socket.on('handle result', (res) => {
      if (typeof res.type !== 'undefined' && res.type === 'hook_response') {
        console.log('hook response===================', res);
        if (typeof res.data.first_name !== 'undefined') {
          this.setState({
            firstName: res.data.first_name,
          });
        }
        if (typeof res.data.last_name !== 'undefined') {
          this.setState({
            lastName: res.data.last_name,
          });
        }
        if (typeof res.data.middle_name !== 'undefined') {
          this.setState({
            middleName: res.data.middle_name,
          });
        }
        if (typeof res.data.address1 !== 'undefined') {
          this.setState({
            address1: res.data.address1,
          });
        }
        if (typeof res.data.address2 !== 'undefined') {
          this.setState({
            address2: res.data.address2,
          });
        }
        if (typeof res.data.address3 !== 'undefined') {
          this.setState({
            address3: res.data.address3,
          });
        }
        if (typeof res.data.date_of_birth !== 'undefined') {
          this.setState({
            dob: res.data.date_of_birth,
          });
        }
        if (typeof res.data.document_numbers.value !== 'undefined') {
          this.setState({
            document: res.data.document_numbers[0].value,
          });
        }
        if (typeof res.data.city !== 'undefined') {
          this.setState({
            city: res.data.city,
          });
        }
        if (typeof res.data.state !== 'undefined') {
          this.setState({
            state: res.data.state,
          });
        }
        if (typeof res.data.country !== 'undefined') {
          this.setState({
            country: res.data.country,
          });
        }
        this.setState({ is_reviewing: true });
      }
    });
  }

  handleSubmit() {
    this.setState({ is_done: true });
  }

  render() {
    const expiredBlock = (
      <Layout>
        <p>
          Your verification token was expired.
          <br />
          Please try again.
          <br />
        </p>
      </Layout>
    );

    const waitingBlock = (
      <Layout>
        <h4>This reviewing progress will take only a few minutes.</h4>
        <h4>Please wait for a moment.</h4>
        <CircularProgress
          className="loader-circle"
          color="inherit"
          onClick={() => this.setState()}
        />
      </Layout>
    );

    const reviewBlock = (
      <Layout style={{ maxWidth: 680, maxHeight: 680 }}>
        <h5 style={{ textAlign: 'center' }}>Please Check for Accuracy</h5>
        <Grid container spacing={4} style={{ paddingTop: 10 }}>
          <Grid item md={6} xs={12}>
            <FormControl fullWidth>
              <TextField
                label="First Name*"
                value={this.state.firstName}
                onChange={(e) => this.setState({ firstName: e.target.value })}
              />
            </FormControl>
          </Grid>
          <Grid item md={6} xs={12}>
            <FormControl fullWidth>
              <TextField
                label="Address 1*"
                value={this.state.address1}
                onChange={(e) => this.setState({ address1: e.target.value })}
              />
            </FormControl>
          </Grid>
          <Grid item md={6} xs={12}>
            <FormControl fullWidth>
              <TextField
                label="Middle Name"
                value={this.state.middleName}
                onChange={(e) => this.setState({ middleName: e.target.value })}
              />
            </FormControl>
          </Grid>
          <Grid item md={6} xs={12}>
            <FormControl fullWidth>
              <TextField
                label="Address 2"
                value={this.state.address2}
                onChange={(e) => this.setState({ address2: e.target.value })}
              />
            </FormControl>
          </Grid>
          <Grid item md={6} xs={12}>
            <FormControl fullWidth>
              <TextField
                label="Last Name*"
                value={this.state.lastName}
                onChange={(e) => this.setState({ lastName: e.target.value })}
              />
            </FormControl>
          </Grid>
          <Grid item md={6} xs={12}>
            <FormControl fullWidth>
              <TextField
                label="Address 3"
                value={this.state.address3}
                onChange={(e) => this.setState({ address3: e.target.value })}
              />
            </FormControl>
          </Grid>
          <Grid item md={6} xs={12}>
            <FormControl fullWidth>
              <TextField
                label="DOB*"
                value={this.state.dob}
                onChange={(e) => this.setState({ dob: e.target.value })}
              />
            </FormControl>
          </Grid>
          <Grid item md={6} xs={12}>
            <FormControl fullWidth>
              <TextField
                label="City*"
                value={this.state.city}
                onChange={(e) => this.setState({ city: e.target.value })}
              />
            </FormControl>
          </Grid>
          <Grid item md={6} xs={12}>
            <FormControl fullWidth>
              <TextField
                label="Document #*"
                value={this.state.document}
                onChange={(e) => this.setState({ document: e.target.value })}
              />
            </FormControl>
          </Grid>
          <Grid item md={3} xs={6}>
            <FormControl fullWidth>
              <TextField
                label="State*"
                value={this.state.state}
                onChange={(e) => this.setState({ state: e.target.value })}
              />
            </FormControl>
          </Grid>
          <Grid item md={3} xs={6}>
            <FormControl fullWidth>
              <TextField
                label="Zip*"
                value={this.state.zip}
                onChange={(e) => this.setState({ zip: e.target.value })}
              />
            </FormControl>
          </Grid>
        </Grid>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <Button
            className="btn-submit"
            variant="contained"
            color="primary"
            disableElevation
            size="large"
            onClick={() => this.handleSubmit()}
          >
            &nbsp;&nbsp;SUBMIT >&nbsp;&nbsp;
          </Button>
        </div>
      </Layout>
    );

    const doneBlock = (
      <Layout>
        <h4 style={{ textAlign: 'center' }}>
          Your personal infos have been submitted.
          <br />
          Thank you
        </h4>
      </Layout>
    );

    if (this.state.is_expired) {
      return expiredBlock;
    }

    if (this.state.is_done) {
      return doneBlock;
    }

    if (this.state.is_reviewing) {
      return reviewBlock;
    } else {
      if (this.state.is_waiting) {
        return waitingBlock;
      } else {
        return <div id="onfido-mount"></div>;
      }
    }
  }
}

export default Process;
